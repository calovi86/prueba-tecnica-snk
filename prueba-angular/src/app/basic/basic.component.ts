import {Component} from '@angular/core';
import {BasicService} from './basic.service';

@Component( {
              templateUrl: './basic.component.html'
            } )
export class BasicComponent {

  constructor(private service: BasicService) {
  }

  // TODO: Completar codigo para llamar al servicio al mostrar el componente
}
