import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BasicComponent} from './basic.component';
import {basicRouting} from './basic.routes';

@NgModule( {
             imports: [CommonModule, basicRouting],
             declarations: [BasicComponent]
           } )
export class BasicModule {

}
