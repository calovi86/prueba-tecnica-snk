import {Component} from '@angular/core';
import {IntermediateService} from './intermediate.service';

@Component( {
              templateUrl: './intermediate.component.html'
            } )
export class IntermediateComponent {

  constructor(private service: IntermediateService) {
  }

  // TODO: Completar codigo para llamar al servicio al mostrar el componente
  readData() {

  }
}
